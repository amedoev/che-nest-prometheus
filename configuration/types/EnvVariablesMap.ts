import VariableOptions from "./VariableOptions";

export default interface EnvVariablesMap {
  [key: string]: VariableOptions;
}
