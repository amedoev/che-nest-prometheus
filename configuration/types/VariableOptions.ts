export default interface VariableOptions {
    name: string;
    defaultValue?: any;
    transform?: (inputValue: any) => any;
}