import { Injectable, NestMiddleware } from '@nestjs/common';
import onFinished from 'on-finished';
import { ServerResponse } from 'http';
import AccessLogger from './AccessLogger';

@Injectable()
class AccessLoggerNestMiddleware implements NestMiddleware {
  constructor(private accessLogger: AccessLogger) {}

  use = (req: Request, res: ServerResponse, next: () => void) => {
    onFinished(res, (_, response: ServerResponse) => {
      this.accessLogger.log(req.method, req.url, response.statusCode, response.statusMessage);
    });

    next();
  };
}

export default AccessLoggerNestMiddleware;
