import Logger from './Logger';
import LogLevel from './LogLevel';


const doNothing: () => void = () => {};

const defaultLogLevel = LogLevel.fatal;


/* eslint-disable no-param-reassign, func-names */
function enableOn(minimalLevel: LogLevel) {
  return function (target: Object, method: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
      // @ts-ignore
      const loggerLevel: LogLevel = this.logLevel || defaultLogLevel;

      if (minimalLevel > loggerLevel) {
        descriptor.value = doNothing;
      } else {
        descriptor.value = originalMethod;
      }

      descriptor.value.apply(this, args);
    };
  };
}
/* eslint-enable no-param-reassign, func-names */

class ConsoleLogger implements Logger {
  readonly logLevel: LogLevel;

  console: Console;

  constructor(logLevel: LogLevel | keyof typeof LogLevel, consoleToUse?: Console) {
    if (typeof logLevel === 'string') {
      this.logLevel = LogLevel.fromString(logLevel) || defaultLogLevel;
    } else {
      this.logLevel = logLevel;
    }

    this.console = consoleToUse || global.console;
  }

  private static getDateTime() {
    return new Date().toISOString();
  }

  public fatal(...args: any[]): void {
    this.console.error(`${ConsoleLogger.getDateTime()} - fatal -`, ...args);
  }

  @enableOn(LogLevel.error)
  public error(...args: any[]): void {
    this.console.error(`${ConsoleLogger.getDateTime()} - error -`, ...args);
  }

  @enableOn(LogLevel.warn)
  public warn(...args: any[]): void {
    this.console.warn(`${ConsoleLogger.getDateTime()} - warn -`, ...args);
  }

  @enableOn(LogLevel.info)
  public info(...args: any[]): void {
    this.console.info(`${ConsoleLogger.getDateTime()} - info -`, ...args);
  }

  @enableOn(LogLevel.debug)
  public debug(...args: any[]): void {
    this.console.debug(`${ConsoleLogger.getDateTime()} - debug -`, ...args);
  }

  @enableOn(LogLevel.trace)
  public trace(...args: any[]): void {
    this.console.trace(`${ConsoleLogger.getDateTime()} - trace -`, ...args);
  }
}

export default ConsoleLogger;
