interface Logger {
  fatal(...args: any[]): void;

  error(...messages: any[]): void;

  warn(...args: any[]): void;

  info(...args: any[]): void;

  debug(...args: any[]): void;

  trace(...args: any[]): void;
}

export default Logger;
