import { LoggerService } from '@nestjs/common';
import ConsoleLogger from './ConsoleLogger';

class NestLogger extends ConsoleLogger implements LoggerService {
  log(message: string) {
    super.info(message);
  }

  error(message: string, trace: string) {
    super.error(message, trace);
  }

  warn(message: string) {
    super.warn(message);
  }

  debug(message: string) {
    super.debug(message);
  }

  verbose(message: string) {
    super.trace(message);
  }
}

export default NestLogger;
