import ConsoleLogger from '../ConsoleLogger';
import LogLevel from '../LogLevel';

const console = {
  error: jest.fn(),
  warn: jest.fn(),
  info: jest.fn(),
  debug: jest.fn(),
  trace: jest.fn(),
} as any;

describe('Console logger', () => {
  describe('Method trace', () => {
    it('console.trace is called(log levels: trace)', async () => {
      const logLevels = [
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.trace();
      });

      expect(console.trace).toHaveBeenCalledTimes(logLevels.length);
    });

    it('console.trace is not called(log levels: fatal, error, warn, info, debug)', async () => {
      const logLevels = [
        LogLevel.fatal,
        LogLevel.error,
        LogLevel.warn,
        LogLevel.info,
        LogLevel.debug,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.trace();
      });

      expect(console.trace).toHaveBeenCalledTimes(0);
    });

    it('call console.trace with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.trace, console);
      const message = 'example';

      consoleLogger.trace(message);

      expect(console.trace).toHaveBeenCalled();
      expect(console.trace)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  describe('Method debug', () => {
    it('console.debug is called(log levels: debug, trace)', async () => {
      const logLevels = [
        LogLevel.debug,
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.debug();
      });

      expect(console.debug).toHaveBeenCalledTimes(logLevels.length);
    });

    it('console.debug is not called(log levels: fatal, error, warn, info)', async () => {
      const logLevels = [
        LogLevel.fatal,
        LogLevel.error,
        LogLevel.warn,
        LogLevel.info,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.debug();
      });

      expect(console.debug).toHaveBeenCalledTimes(0);
    });

    it('call console.debug with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.debug, console);
      const message = 'example';

      consoleLogger.debug(message);

      expect(console.debug).toHaveBeenCalled();
      expect(console.debug)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  describe('Method info', () => {
    it('console.info is called(log levels: info, debug, trace)', async () => {
      const logLevels = [
        LogLevel.info,
        LogLevel.debug,
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.info();
      });

      expect(console.info).toHaveBeenCalledTimes(logLevels.length);
    });

    it('console.info is not called(log levels: fatal, error, warn)', async () => {
      const logLevels = [
        LogLevel.fatal,
        LogLevel.error,
        LogLevel.warn,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.info();
      });

      expect(console.info).toHaveBeenCalledTimes(0);
    });

    it('call console.info  with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.info, console);
      const message = 'example';

      consoleLogger.info(message);

      expect(console.info).toHaveBeenCalled();
      expect(console.info)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  describe('Method warn', () => {
    it('console.warn is called(log levels: warn, info, debug, trace)', async () => {
      const logLevels = [
        LogLevel.warn,
        LogLevel.info,
        LogLevel.debug,
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.warn();
      });

      expect(console.warn).toHaveBeenCalledTimes(logLevels.length);
    });

    it('console.warn is not called(log levels: fatal, error)', async () => {
      const logLevels = [
        LogLevel.fatal,
        LogLevel.error,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.warn();
      });

      expect(console.warn).toHaveBeenCalledTimes(0);
    });

    it('call console.warn  with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.warn, console);
      const message = 'example';

      consoleLogger.warn(message);

      expect(console.warn).toHaveBeenCalled();
      expect(console.warn)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  describe('Method error', () => {
    it('console.error is called(log levels: error, warn, info, debug, trace)', async () => {
      const logLevels = [
        LogLevel.error,
        LogLevel.warn,
        LogLevel.info,
        LogLevel.debug,
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.error();
      });

      expect(console.error).toHaveBeenCalledTimes(logLevels.length);
    });

    it('console.error is not called(log levels: fatal)', async () => {
      const logLevels = [
        LogLevel.fatal,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.error();
      });

      expect(console.error).toHaveBeenCalledTimes(0);
    });

    it('call console.error with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.error, console);
      const message = 'example';

      consoleLogger.error(message);

      expect(console.error).toHaveBeenCalled();
      expect(console.error)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  describe('Method fatal', () => {
    it('console.fatal is called(log levels: fatal, error, warn, info, debug, trace)', async () => {
      const logLevels = [
        LogLevel.fatal,
        LogLevel.error,
        LogLevel.warn,
        LogLevel.info,
        LogLevel.debug,
        LogLevel.trace,
      ];

      logLevels.forEach((logLevel) => {
        const consoleLogger = new ConsoleLogger(logLevel, console);

        consoleLogger.fatal();
      });

      expect(console.error).toHaveBeenCalledTimes(logLevels.length);
    });

    it('call console.fatal with parameter ("message": "example")', async () => {
      const consoleLogger = new ConsoleLogger(LogLevel.fatal, console);
      const message = 'example';

      consoleLogger.fatal(message);

      expect(console.error).toHaveBeenCalled();
      expect(console.error)
        .toHaveBeenCalledWith(expect.anything(), message);
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
