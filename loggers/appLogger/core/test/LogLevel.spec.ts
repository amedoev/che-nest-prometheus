import LogLevel from '../LogLevel';

describe('Log level', () => {
  describe('Method getLevelFromString', () => {
    const logLevelTestsData = [
      { logLevelString: 'fatal', logLevelResult: LogLevel.fatal },
      { logLevelString: 'error', logLevelResult: LogLevel.error },
      { logLevelString: 'warn', logLevelResult: LogLevel.warn },
      { logLevelString: 'debug', logLevelResult: LogLevel.debug },
      { logLevelString: 'trace', logLevelResult: LogLevel.trace },
      { logLevelString: 'info', logLevelResult: LogLevel.info },
      { logLevelString: 'random string', logLevelResult: LogLevel.info },
    ];

    logLevelTestsData.forEach((logLevelTestData) => {
      it(`get level from "${logLevelTestData.logLevelString}"`, async () => {
        const logLevel = LogLevel.fromString(logLevelTestData.logLevelString);

        expect(logLevel).toBe(logLevelTestData.logLevelResult);
      });
    });
  });
});
