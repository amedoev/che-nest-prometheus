import {
  Histogram,
  Summary,
} from 'prom-client';

interface RequestMetrics {
  histogram: Histogram;
  summary: Summary;
}

export default RequestMetrics;
