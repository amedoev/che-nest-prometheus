import PromServerConfig from '../types/PromServerConfig';
import ConfigurationHelper from 'che-configuration-helper';

const config = ConfigurationHelper.getEnvVariables<PromServerConfig>({
  port: { name: 'PROMETHEUS_PORT', transform: Number, defaultValue: 9090 },
});

export default config;
